"""
1、安装模块 pip install pypiwin32
"""

import win32com.client
import os


def translate():
    ppt_app = win32com.client.Dispatch('PowerPoint.Application')
    ppt_dir_path = 'D:\\3pyoffice\\ppt2pdf'
    pdf_dir_path = 'D:\\3pyoffice\\ppt2pdf'
    try:
        filenames = os.listdir(ppt_dir_path)
        for filename in filenames:
            if filename.endswith('ppt') or filename.endswith('pptx'):
                base, ext = filename.split('.')
                output_filename = pdf_dir_path + '/' + base + '.pdf'
                filename = ppt_dir_path + '/' + filename
                ppt = ppt_app.Presentations.Open(filename)
                ppt.SaveAs(output_filename, 32)
    except Exception as e:
        print(e)
    finally:
        ppt_app.Quit()


if __name__ == '__main__':
    translate()
